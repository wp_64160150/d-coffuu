export default interface Menu {
  id: number;
  type: string;
  name: string;
  price: number;
  pic: string;
  qty: number;
}
