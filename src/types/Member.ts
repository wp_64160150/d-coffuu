export default interface Member {
  id: number;
  name: string;
  phone: string;
}
